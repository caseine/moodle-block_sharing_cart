<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin local library.
 *
 * @package    block_sharing_cart
 * @copyright  2022 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function block_sharing_cart_is_visitor($context) {
    global $DB, $USER;
    // Always display the Sharing cart when on visitor role.
    $visitorroleid = $DB->get_field('role', 'id', array('id' => get_config('block_sharing_cart', 'visitorrole')));

    return $visitorroleid !== false && (user_has_role_assignment($USER->id, $visitorroleid, $context->id)
            || !empty(get_user_accessdata($USER->id)['ra'][$context->path][$visitorroleid])
            || (get_user_accessdata($USER->id)['rsw'][$context->path] ?? -1) == $visitorroleid);
}

function block_sharing_cart_get_unshared_modules($courseid) {
    global $DB;
    $metadataplugin = core_component::get_plugin_directory('local', 'metadata');
    if ($metadataplugin !== null) {
        $sql = "SELECT DISTINCT cm.id
                        FROM {course_modules} cm
                        JOIN {local_metadata_field} mdf ON mdf.id = :fieldid
                        LEFT JOIN {local_metadata} md ON md.instanceid = cm.id AND md.fieldid = mdf.id
                        WHERE cm.course = :courseid
                            AND COALESCE(md.data,0) <> 1";
        return $DB->get_records_sql($sql, array('courseid' => $courseid, 'fieldid' => get_config('block_sharing_cart', 'metadatasharedfield')));
    } else {
        return array();
    }
}