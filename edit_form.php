<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die;

class block_sharing_cart_edit_form extends block_edit_form {

    /**
     * Configuration page
     *
     * @param MoodleQuickForm $mform
     */
    protected function specific_definition($mform) {
        global $DB, $COURSE, $USER;
        $metadataplugin = core_component::get_plugin_directory('local', 'metadata');
        $visitorrole = $DB->get_record('role', array('id' => get_config('block_sharing_cart', 'visitorrole')));
        if ($metadataplugin !== null && $visitorrole !== false && get_config('block_sharing_cart', 'metadatasharedfield') > '') {
            $context = context_course::instance($COURSE->id);
            $visitor = role_get_name($visitorrole, $context);
            $mform->addElement('header', 'sharingheader', get_string('edit:sharingheader', 'block_sharing_cart', $visitor));
            $mform->addElement('advcheckbox', 'config_preventunsharedbackup', get_string('edit:preventunsharedbackupfrom', 'block_sharing_cart', $visitor));
            $mform->addHelpButton('config_preventunsharedbackup', 'edit:preventunsharedbackup', 'block_sharing_cart');
            $mform->setDefault('config_preventunsharedbackup', 1);

            $mform->addElement('advcheckbox', 'config_removeunsharedlinks', get_string('edit:removeunsharedlinksfor', 'block_sharing_cart', $visitor));
            $mform->addHelpButton('config_removeunsharedlinks', 'edit:removeunsharedlinks', 'block_sharing_cart');
            $mform->setDefault('config_removeunsharedlinks', 1);
            $mform->disabledIf('config_removeunsharedlinks', 'config_preventunsharedbackup', 'notchecked');

            $potentialcontacts = array_map('fullname', get_users_by_capability($context, 'moodle/course:manageactivities', 'u.id,' . get_all_user_name_fields(true, 'u')));
            $mform->addElement('select', 'config_contact', get_string('edit:sendmessageto', 'block_sharing_cart', $visitor),
                    array('' => get_string('none'), $USER->id => fullname($USER)) + $potentialcontacts);
            $mform->addHelpButton('config_contact', 'edit:sendmessageto', 'block_sharing_cart');
            $mform->setDefault('config_contact', '');
        }
    }

}
